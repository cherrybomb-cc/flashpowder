<?php

/**
 |-----------------------------------------------------------
 | Theme Actions
 |-----------------------------------------------------------
 |
 | This file purpose is to include your custom
 | actions hooks, which process a various
 | logic at specific parts of WordPress.
 |
 */

namespace Flashpowder\Theme\App\Setup;

use function Flashpowder\Theme\App\asset_path;

/**
 * Header Cleanup
 *
 * Site <head> optimization, dead code elimination
 */
function cleanup()
{
    // Some unnecesary data.
    remove_action( 'wp_head', 'wp_generator' );
    remove_action( 'wp_head', 'wlwmanifest_link' );
    remove_action( 'wp_head', 'rsd_link' );

    // All actions related to emojis.
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

    add_filter( 'emoji_svg_url', '__return_false' );

    // Removes links to rss categories.
    remove_action( 'wp_head', 'feed_links_extra', 3 );

    // Minus links to the main rss and comments.
    remove_action( 'wp_head', 'feed_links', 2 );

    // Remove the REST API endpoint.
    remove_action( 'rest_api_init', 'wp_oembed_register_route' );

    // Remove JSON API links.
    remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
    remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );

    // Turn off oEmbed auto discovery.
    // Don't filter oEmbed results.
    remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );

    // Remove oEmbed discovery links.
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );

    // Remove oEmbed-specific JavaScript from the front-end and back-end.
    remove_action( 'wp_head', 'wp_oembed_add_host_js' );

    // Blog posts links.
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
    remove_action( 'wp_head', 'index_rel_link' );
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
    remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );

    // Remove SVG Filters from WP 5.9.1.
    remove_action( 'wp_body_open', 'wp_global_styles_render_svg_filters' );

    global $wp_widget_factory;
    remove_action(
        'wp_head',
        [
            $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
            'recent_comments_style',
        ]
    );
}
add_action( 'init', __NAMESPACE__ . '\\cleanup' );

/**
 * Remove non-essential panels from customizer
 *
 * @param \WP_Customize_Manager $wp_customize The manager instance.
 */
function remove_customizer_sections( $wp_customize )
{
    $wp_customize->remove_section( 'static_front_page' );
    $wp_customize->remove_section( 'custom_css' );
}
add_action( 'customize_register', __NAMESPACE__ . '\\remove_customizer_sections' );

/**
 * Preload critic assets such as fonts, images, etc.
 *
 * @uses Flashpowder\Theme\App\asset_path
 */
function preload_assets() {
    $fonts = [];

    foreach ( $fonts as $font ) {
        // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
        echo sprintf( '<link rel="preload" href="%s" as="font" type="font/woff2" crossorigin>', $font ) . PHP_EOL;
    }

    $images = [];

    foreach ( $images as $image_id ) {
        // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
        echo sprintf( '<link rel="preload" href="%s" as="image" type="image/jpg">', wp_get_attachment_image_src( $image_id, 'full' )[0] ) . PHP_EOL;
    }
}
add_action( 'wp_head', __NAMESPACE__ . '\\preload_assets', 7 );

/**
 * Feature detection tests and initial scripting
 */
function feature_detection_tests()
{
    //window.appStartTime = performance.now();
    //(!!window.MSInputMethodContext && !!document.documentMode) && document.documentElement.classList.add('is-ie11');
    //(!!window.IntersectionObserver) && document.documentElement.classList.add('has-intersection-observer');
    echo <<<JS
<script>
document.documentElement.classList.remove('no-js');
const appCss = document.getElementById('app-css');
appCss.onload = function() {
    appCss.onload = null;
    appCss.isLoaded = true;
}
</script>\n
JS;
}
add_action( 'wp_head', __NAMESPACE__ . '\\feature_detection_tests', 100 );

/**
 * Embeds SVG symbols file into the document
 *
 * @see resources/templates/layout/head.tpl.php
 */
function embed_svg_sprites()
{
    $load_svg = function ( $file ) {
        $svg_sprites = asset_path( $file );
        $svg_sprites_headers = @get_headers( $svg_sprites );
        if ( stripos( $svg_sprites_headers[0], '200 OK' ) ) {
            // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
            echo file_get_contents( $svg_sprites );
        } else {
            // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
            echo "<!-- Could not embed {$file}, see log for details -->" . PHP_EOL;
        }
    };

    $load_svg( 'images/sprites.svg' );
}
add_action( 'wp_body_open', __NAMESPACE__ . '\\embed_svg_sprites' );
