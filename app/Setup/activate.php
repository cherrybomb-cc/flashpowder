<?php

/**
 |-----------------------------------------------------------
 | Theme Activation
 |-----------------------------------------------------------
 |
 | This file purpose is to hold the functions
 | that will be run on theme activation.
 |
 */

namespace Flashpowder\Theme\App\Setup;

/**
 * Create a default menu if there's no menu
 *
 * @return void
 */
function theme_activation()
{
    // Check if the menu exists.
    $menu_name   = 'Main Menu';
    $menu_exists = wp_get_nav_menu_object( $menu_name );

    // If it doesn't exist, let's create it.
    if ( ! $menu_exists ) {
        $menu_id = wp_create_nav_menu( $menu_name );

        // Set up default menu items.
        wp_update_nav_menu_item(
            $menu_id,
            0,
            [
                'menu-item-title' => __( 'Home' ),
                'menu-item-classes' => 'menu-item-home',
                'menu-item-url' => home_url( '/' ),
                'menu-item-status' => 'publish',
                'menu-item-type' => 'custom',
            ]
        );

        $pages = get_pages();

        foreach ( $pages as $page ) {
            wp_update_nav_menu_item(
                $menu_id,
                0,
                [
                    'menu-item-object-id' => $page->ID,
                    'menu-item-object' => $page->post_type,
                    'menu-item-type' => 'post_type',
                    'menu-item-status' => 'publish',
                ]
            );
        }

        // Set the new menu as the primary.
        $locations = get_theme_mod( 'nav_menu_locations' );
        $locations['primary'] = $menu_id;
        set_theme_mod( 'nav_menu_locations', $locations );
    }
}
add_action( 'after_switch_theme', __NAMESPACE__ . '\\theme_activation' );
