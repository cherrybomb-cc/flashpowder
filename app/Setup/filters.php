<?php

/**
 |-----------------------------------------------------------
 | Theme Filters
 |-----------------------------------------------------------
 |
 | This file purpose is to include your theme various
 | filters hooks, which changes output or behaviour
 | of different parts of WordPress functions.
 |
 */

namespace Flashpowder\Theme\App\Setup;

/**
 * Hides sidebar on index template on specific views.
 *
 * @param bool $status The default status to consider.
 *
 * @see apply_filters( 'theme/single/sidebar/visibility' )
 */
function show_index_sidebar( $status )
{
    if ( is_404() || is_page() ) {
        return false;
    }

    return $status;
}
add_filter( 'theme/single/sidebar/visibility', __NAMESPACE__ . '\\show_index_sidebar' );

/**
 * Shortens posts excerpts to 60 words.
 *
 * @return integer
 */
function modify_excerpt_length()
{
    return 60;
}
add_filter( 'excerpt_length', __NAMESPACE__ . '\\modify_excerpt_length' );

/**
 * Clean enqueued stylesheet <link> tags and add `isLoaded`
 * javascript attribute to the main styles element tag.
 *
 * @param string $input The HTML for the style tag.
 */
function clean_style_tag( $input )
{
    // phpcs:disable WordPress.WP.EnqueuedResources.NonEnqueuedStylesheet
    preg_match_all( "!<link rel='stylesheet' id='([^']+)' href='(.*)'(?: type='text/css')? media='(.*)' />!", $input, $matches );
    if ( empty( $matches[2] ) ) {
        return $input;
    }

    $style_tag = '<link rel="stylesheet" id="%s" href="%s" media="%s">';
    //if ( 'app-css' === $matches[1][0] ) {
    //    $style_tag = '<link rel="stylesheet" id="%s" href="%s" media="%s" onload="this.onload = null; this.isLoaded = true">';
    //}
    // phpcs:enable WordPress.WP.EnqueuedResources.NonEnqueuedStylesheet

    return sprintf(
        $style_tag,
        $matches[1][0],
        $matches[2][0],
        $matches[3][0]
    ) . PHP_EOL;
}
add_filter( 'style_loader_tag', __NAMESPACE__ . '\\clean_style_tag' );
