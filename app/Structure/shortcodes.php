<?php

/**
 |--------------------------------------------------------------------------
 | Custom Shortcodes
 |--------------------------------------------------------------------------
 |
 | This file is for registering your shortcodes. Shortcodes allows to facade
 | a code logic behind readable piece of text. Below you have an example
 | which facilitates outputing markup with template() helper function.
 |
 */

namespace Flashpowder\Theme\App\Structure;

use function Flashpowder\Theme\App\template;

/**
 * Renders a [button] shortcode.
 *
 * @param array  $atts Array of attributes passed from the shortcode.
 * @param string $content The content inside the shortcode.
 *
 * @return string
 */
function render_button_shortcode( $atts, $content )
{
    $attributes = shortcode_atts(
        [
            'href' => '#',
        ],
        $atts
    );

    ob_start();

    template( 'shortcodes/button', compact( 'attributes', 'content' ) );

    return ob_get_clean();
}
add_shortcode( 'button', __NAMESPACE__ . '\\render_button_shortcode' );
