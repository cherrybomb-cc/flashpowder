<?php

/**
 |----------------------------------------------------------------
 | Menu Walker
 |----------------------------------------------------------------
 |
 | This walker helps to get a clean simple markup
 | for the navigation.
 |
 */

namespace Flashpowder\Theme\App\Structure;

/**
 * Menu Walker Class
 */
class Menu_Walker extends \Walker_Nav_Menu {
    /**
     * Walk through all the items and its descendants.
     *
     * @param array $elements An array of menu items.
     * @param int   $max_depth The maximum hierarchical depth.
     * @param mixed ...$args Optional additional arguments.
     *
     * @return string
     */
    public function walk( $elements, $max_depth, ...$args )
    {
        $list = [];
        $menu_class = $args[0]->menu_class;

        foreach ( $elements as $item ) {
            $class_attr = $menu_class . '_item';
            if ( $item->current ) {
                $class_attr .= ' -current';
            }
            $list[] = sprintf( '<li class="%s"><a href="%s" data-load-transition="nav-menu">%s</a></li>', $class_attr, $item->url, $item->title );
        }

        return join( "\n", $list );
    }
}
