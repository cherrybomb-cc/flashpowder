<?php

/**
 |-----------------------------------------------------------
 | Theme Templates Actions
 |-----------------------------------------------------------
 |
 | This file purpose is to include your templates rendering
 | actions hooks, which allows you to render specific
 | partials at specific places of your theme.
 |
 */

namespace Flashpowder\Theme\App\Structure;

use function Flashpowder\Theme\App\template;

/**
 * Renders post thumbnail by its formats.
 *
 * @see resources/templates/index.tpl.php
 */
function render_post_thumbnail()
{
    template( [ 'partials/post/thumbnail', get_post_format() ] );
}
add_action( 'theme/index/post/thumbnail', __NAMESPACE__ . '\\render_post_thumbnail' );

/**
 * Renders empty post content where there is no posts.
 *
 * @see resources/templates/index.tpl.php
 */
function render_empty_content()
{
    template( [ 'partials/index/content', 'none' ] );
}
add_action( 'theme/index/content/none', __NAMESPACE__ . '\\render_empty_content' );

/**
 * Renders post contents by its formats.
 *
 * @see resources/templates/single.tpl.php
 */
function render_post_content()
{
    template( [ 'partials/post/content', get_post_format() ] );
}
add_action( 'theme/single/content', __NAMESPACE__ . '\\render_post_content' );

/**
 * Renders sidebar content.
 *
 * @uses resources/templates/partials/sidebar.tpl.php
 * @see resources/templates/single.tpl.php
 */
function render_sidebar()
{
    get_sidebar();
}
add_action( 'theme/single/sidebar', __NAMESPACE__ . '\\render_sidebar' );
