<?php

/**
 |----------------------------------------------------------------
 | Theme Navigation Areas
 |----------------------------------------------------------------
 |
 | This file is for registering your theme custom navigation areas
 | where various menus can be assigned by site administrators.
 |
 */

namespace Flashpowder\Theme\App\Structure;

/**
 * Registers navigation areas.
 *
 * @return void
 */
function register_navigation_areas()
{
    register_nav_menus(
        [
            'primary' => __( 'Primary', 'flashpowder' ),
        ]
    );
}
add_action( 'after_setup_theme', __NAMESPACE__ . '\\register_navigation_areas' );
