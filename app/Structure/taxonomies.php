<?php

/**
 |-----------------------------------------------------------
 | Theme Custom Taxonomies
 |-----------------------------------------------------------
 |
 | This file is for registering your theme custom taxonomies.
 | Taxonomies help to classify posts and custom post types.
 |
 */

namespace Flashpowder\Theme\App\Structure;

/**
 * Registers `book_genre` custom taxonomy.
 *
 * @return void
 */
function register_book_genre_taxonomy()
{
    register_taxonomy(
        'book_genre',
        'book',
        [
            'rewrite' => [
                'slug' => 'books/genre',
                'with_front' => true,
                'hierarchical' => true,
            ],
            'hierarchical' => true,
            'public' => true,
            'labels' => [
                'name' => _x( 'Genres', 'taxonomy general name', 'flashpowder' ),
                'singular_name' => _x( 'Genre', 'taxonomy singular name', 'flashpowder' ),
                'search_items' => __( 'Search Genres', 'flashpowder' ),
                'all_items' => __( 'All Genres', 'flashpowder' ),
                'parent_item' => __( 'Parent Genre', 'flashpowder' ),
                'parent_item_colon' => __( 'Parent Genre:', 'flashpowder' ),
                'edit_item' => __( 'Edit Genre', 'flashpowder' ),
                'update_item' => __( 'Update Genre', 'flashpowder' ),
                'add_new_item' => __( 'Add New Genre', 'flashpowder' ),
                'new_item_name' => __( 'New Genre Name', 'flashpowder' ),
                'menu_name' => __( 'Genre', 'flashpowder' ),
            ],
        ]
    );
}
add_action( 'init', __NAMESPACE__ . '\\register_book_genre_taxonomy' );
