<?php

/**
 |-----------------------------------------------------------
 | Custom Thumbnails Sizes
 |-----------------------------------------------------------
 |
 | This file is for registering your custom
 | image sizes, for posts thumbnails.
 |
 */

namespace Flashpowder\Theme\App\Structure;

/**
 * Disable the scaling of images larger than 2560px.
 */
add_filter( 'big_image_size_threshold', '__return_false' );

/**
 * Improve image compression.
 */
add_filter(
    'jpeg_quality',
    function () {
        return 95;
    }
);

/**
 * Adjust compression quality for uploaded images.
 *
 * @param int    $quality Quality level between 1 and 100.
 * @param string $mime_type Image mime type.
 *
 * @return int
 */
function filter_image_quality( $quality, $mime_type )
{
    if ( 'image/webp' === $mime_type ) {
        return 96;
    }

    return $quality;
}
add_filter( 'wp_editor_set_quality', __NAMESPACE__ . '\\filter_image_quality', 10, 2 );

/**
 * Adds new thumbnails image sizes.
 *
 * @return void
 */
function add_image_sizes()
{
    // phpcs:disable
    //remove_image_size( '1536x1536' );
    //remove_image_size( '2048x2048' );

    //add_image_size( 'largest-panoramic', 2560, 2560 );

    // Add size to the list in Gutenberg
    //add_filter( 'image_size_names_choose', function( $sizes ) {
    //    return array_merge( $sizes, [
    //        'portfolio-content' => __( 'Work image' ),
    //    ] );
    //} );
    // phpcs:enable
}
add_action( 'after_setup_theme', __NAMESPACE__ . '\\add_image_sizes' );

/**
 * Remove unused default WordPress default thumbnail sizes.
 *
 * @param array $sizes An associative array of image sizes.
 *
 * @return array
 */
function remove_default_media_sizes( $sizes ) {

    // Default WordPress sizes.
    // unset( $sizes[ 'thumbnail' ] );       // Remove Thumbnail (150 x 150 hard cropped).
    // unset( $sizes[ 'medium' ] );          // Remove Medium resolution (300 x 300 max height 300px).
    // unset( $sizes[ 'medium_large' ] );    // Remove Medium Large (added in WP 4.4) resolution (768 x 0 infinite height).
    // unset( $sizes[ 'large' ] );           // Remove Large resolution (1024 x 1024 max height 1024px).

    // unset( $sizes[ '1536x1536' ] );       // Added in version 5.3.
    unset( $sizes[ '2048x2048' ] );       // Added in version 5.3.

    return $sizes;
}
add_action( 'intermediate_image_sizes_advanced', __NAMESPACE__ . '\\remove_default_media_sizes' );
