<?php

/**
 |-----------------------------------------------------------
 | Theme Custom Widgets
 |-----------------------------------------------------------
 |
 | This file is for registering your theme widgets, so
 | they can be assigned to sidebar areas in admin
 | panel by website administrator or user.
 |
 */

namespace Flashpowder\Theme\App\Structure;

/**
 * Registers custom widgets.
 *
 * @return void
 */
function register_widgets()
{
    // Use: register_widget('\My\Widget\Class'); to load the class.
}
add_action( 'widgets_init', __NAMESPACE__ . '\\register_widgets' );
