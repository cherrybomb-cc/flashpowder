<?php

/**
 |-----------------------------------------------------------
 | Theme Sidebars
 |-----------------------------------------------------------
 |
 | This file is for registering your theme sidebars,
 | Creates widgetized areas, which an administrator
 | of the site can customize and assign widgets.
 |
 */

namespace Flashpowder\Theme\App\Structure;

/**
 * Registers the widget areas.
 *
 * @return void
 */
function register_widget_areas()
{
    register_sidebar(
        [
            'id' => 'sidebar',
            'name' => __( 'Sidebar', 'flashpowder' ),
            'description' => __( 'Website sidebar', 'flashpowder' ),
            'before_title' => '<h5>',
            'after_title' => '</h5>',
        ]
    );
}
add_action( 'widgets_init', __NAMESPACE__ . '\\register_widget_areas' );
