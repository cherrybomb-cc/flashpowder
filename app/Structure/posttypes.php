<?php

/**
 |-----------------------------------------------------------
 | Theme Custom Post Types
 |-----------------------------------------------------------
 |
 | This file is for registering your theme post types.
 | Custom post types allow users to easily create
 | and manage various types of content.
 |
 */

namespace Flashpowder\Theme\App\Structure;

/**
 * Registers `book` custom post type.
 *
 * @return void
 */
function register_book_post_type()
{
    $labels = [
        'name'                  => _x( 'Books', 'Post type general name', 'flashpowder' ),
        'singular_name'         => _x( 'Book', 'Post type singular name', 'flashpowder' ),
        'menu_name'             => _x( 'Books', 'Admin Menu text', 'flashpowder' ),
        'name_admin_bar'        => _x( 'Book', 'Add New on Toolbar', 'flashpowder' ),
        'add_new'               => __( 'Add New', 'flashpowder' ),
        'add_new_item'          => __( 'Add New Book', 'flashpowder' ),
        'new_item'              => __( 'New Book', 'flashpowder' ),
        'edit_item'             => __( 'Edit Book', 'flashpowder' ),
        'view_item'             => __( 'View Book', 'flashpowder' ),
        'all_items'             => __( 'All Books', 'flashpowder' ),
        'search_items'          => __( 'Search Books', 'flashpowder' ),
        'parent_item_colon'     => __( 'Parent Books:', 'flashpowder' ),
        'not_found'             => __( 'No books found.', 'flashpowder' ),
        'not_found_in_trash'    => __( 'No books found in Trash.', 'flashpowder' ),
        'featured_image'        => _x( 'Book Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'flashpowder' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'flashpowder' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'flashpowder' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'flashpowder' ),
        'archives'              => _x( 'Book archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'flashpowder' ),
        'insert_into_item'      => _x( 'Insert into book', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'flashpowder' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this book', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'flashpowder' ),
        'filter_items_list'     => _x( 'Filter books list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'flashpowder' ),
        'items_list_navigation' => _x( 'Books list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'flashpowder' ),
        'items_list'            => _x( 'Books list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'flashpowder' ),
    ];

    $args = [
        'description'        => __( 'Collection of books.', 'flashpowder' ),
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => [ 'slug' => 'book' ],
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => [ 'title', 'editor', 'excerpt', 'thumbnail' ],
    ];

    register_post_type( 'book', $args );
}
add_action( 'init', __NAMESPACE__ . '\\register_book_post_type' );
