<?php

/**
 |-----------------------------------------------------------------
 | Theme Assets
 |-----------------------------------------------------------------
 |
 | This file is for registering your theme stylesheets and scripts.
 | In here you should also deregister all unwanted assets which
 | can be shiped with various third-parity plugins.
 |
 */

namespace Flashpowder\Theme\App\Http;

use function Flashpowder\Theme\App\asset_path;

/**
 * Remove unnecesary default assets
 *
 * @return void
 */
function assets_cleanup()
{
    // Remove Block Library CSS.
    wp_dequeue_style( 'wp-block-library' );

    // Remove Global Styles from WP 5.9.1.
    wp_dequeue_style( 'global-styles' );

    // WordPress core styles.
    wp_dequeue_style( 'classic-theme-styles' );
    // wp_dequeue_style('wp-block-library'); .
    // wp_dequeue_style('wp-block-library-theme'); .

    // WooCommerce
    // wp_dequeue_style('wc-block-style'); .
}
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\assets_cleanup' );

/**
 * Registers theme stylesheet files.
 *
 * @return void
 */
function register_stylesheets()
{
    wp_enqueue_style( 'app', asset_path( 'css/app.css' ) );
}
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\register_stylesheets' );

/**
 * Registers theme script files.
 *
 * @return void
 */
function register_scripts()
{
    wp_enqueue_script( 'app', asset_path( 'scripts/app.js' ), [ 'jquery' ], null, true );
}
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\register_scripts' );

/**
 * Registers editor stylesheets.
 *
 * @return void
 */
function register_editor_stylesheets()
{
    add_editor_style( asset_path( 'css/app.css' ) );
}
add_action( 'admin_init', __NAMESPACE__ . '\\register_editor_stylesheets' );

/**
 * Remove jQuery Migrate plugin
 *
 * @param \WP_Scripts $wp_scripts The global scripts instance object.
 */
function remove_jquery_migrate( $wp_scripts )
{
    if ( ! is_admin() && isset( $wp_scripts->registered['jquery'] ) ) {
        $script = $wp_scripts->registered['jquery'];
        if ( $script->deps ) {
            $script->deps = array_diff( $script->deps, [ 'jquery-migrate' ] );
        }
    }
}
add_action( 'wp_default_scripts', __NAMESPACE__ . '\\remove_jquery_migrate' );

/**
 * Moves front-end jQuery script to the footer.
 *
 * @param  \WP_Scripts $wp_scripts The global scripts instance object.
 * @return void
 */
function move_jquery_to_the_footer( $wp_scripts )
{
    if ( ! is_admin() ) {
        $wp_scripts->add_data( 'jquery', 'group', 1 );
        $wp_scripts->add_data( 'jquery-core', 'group', 1 );
        // Another script could be: 'jquery-migrate'.
    }
}
add_action( 'wp_default_scripts', __NAMESPACE__ . '\\move_jquery_to_the_footer' );
