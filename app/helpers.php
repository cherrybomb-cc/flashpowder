<?php

/**
|--------------------------------------------------------------------------
| Theme Helpers
|--------------------------------------------------------------------------
|
| Here lives all the helper functions that are used along the theme.
|
 */

namespace Flashpowder\Theme\App;

use Flashpowder\Core\Asset\Asset;
use Flashpowder\Core\Foundation\Theme;
use Flashpowder\Core\Template\Template;

/**
 * Gets theme instance.
 *
 * @param string|null $key Name of the service to be retrieed.
 * @param array       $parameters Dataset passed to the service.
 *
 * @return \Flashpowder\Core\Foundation\Theme
 */
function theme( $key = null, $parameters = [] )
{
    if ( null !== $key ) {
        return Theme::getInstance()->get( $key, $parameters );
    }

    return Theme::getInstance();
}

/**
 * Gets theme config instance.
 *
 * @param string|null $key The name of the config item to retrieve.
 *
 * @return array
 */
function config( $key = null )
{
    if ( null !== $key ) {
        return theme( 'config' )->get( $key );
    }

    return theme( 'config' );
}

/**
 * Renders template file with data.
 *
 * @param  string $file Relative path to the template file.
 * @param  array  $data Dataset for the template.
 *
 * @return void
 */
function template( $file, $data = [] )
{
    $template = new Template( config() );

    $template
        ->setFile( $file )
        ->render( $data );
}

/**
 * Returns the template content
 *
 * @param string $file Relative path to the template file.
 * @param array  $data Dataset for the template.
 *
 * @return string
 */
function get_template( $file, $data = [] )
{
    $template = new Template( config() );

    ob_start();
    $template
        ->setFile( $file )
        ->render( $data );

    return ob_get_clean();
}

/**
 * Gets asset instance.
 *
 * @param  string $file Relative file path to the asset file.
 *
 * @return \Flashpowder\Core\Asset\Asset
 */
function asset( $file )
{
    $asset = new Asset( config() );

    return $asset->setFile( $file );
}

/**
 * Gets asset file from public directory.
 *
 * @param  string $file Relative file path to the asset file.
 *
 * @return string
 */
function asset_path( $file )
{
    return asset( $file )->getUri();
}

/**
 * Get the current template.
 */
function get_current_template()
{
    global $template;
    return basename( $template, '.php' );
}

