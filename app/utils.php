<?php

/**
|--------------------------------------------------------------------------
| Theme Utils
|--------------------------------------------------------------------------
|
| Here are all the utility functions that are used along the theme.
|
 */

namespace Flashpowder\Theme\App;

/**
 * Mail obfuscation
 *
 * @see http://www.maurits.vdschee.nl/php_hide_email/
 *
 * @param string $email The email to obfuscate.
 * @param string $text Text to use instead of the email itself.
 *
 * @return string
 */
function hide_email( $email, $text = '' )
{
    $character_set = '+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';
    $key = str_shuffle( $character_set );
    $cipher_text = '';
    $id = 'e' . rand( 1, 999999999 );
    $email_length = strlen( $email );

    for ( $i = 0; $i < $email_length; ++$i ) {
        $cipher_text .= $key[ strpos( $character_set, $email[ $i ] ) ];
    }

    $output = <<<JAVASCRIPT
(() => {
  const a='$key';
  const b=a.split('').sort().join('');
  const c='$cipher_text';
  const d='$text';
  let e='';
  for(let f=0;f<c.length;f++)e+=b.charAt(a.indexOf(c.charAt(f)));
  document.getElementById('$id').innerHTML=`<a href="mailto:\${e}">\${d||e}</a>`;
})()
JAVASCRIPT;

    $output = str_replace( "\n", '', $output );
    $output = sprintf( '<span id="%s">[javascript protected email address]</span><script>%s</script>', $id, $output );

    return $output;
}
