<?php

/**
|------------------------------------------------------------------
| Template
|------------------------------------------------------------------
|
| This file helps for managing templates located in the
| `resources/templates` folder.
|
 */

namespace Flashpowder\Core\Template;

use Flashpowder\Core\Contract\ConfigInterface;
use Flashpowder\Core\Exception\FileNotFoundException;

/**
 * Template Class
 *
 * This class helps to localize and retrieve template files
 * with optional passed data to it.
 */
class Template {
    /**
     * Theme config instance.
     *
     * @var Flashpowder\Core\Contract\ConfigInterface
     */
    protected $config;

    /**
     * File path to the template.
     *
     * @var string
     */
    protected $file;

    /**
     * Construct template.
     *
     * @param Flashpowder\Core\Contract\ConfigInterface $config The config instance.
     */
    public function __construct( ConfigInterface $config ) {
        $this->config = $config;
    }

    /**
     * Render template.
     *
     * @param array $context The passed data to the template.
     *
     * @throws Flashpowder\Core\Exception\FileNotFoundException When the template is not found.
     *
     * @return void
     */
    public function render( array $context = [] ) {
        $template = locate_template( $this->getRelativePath(), false, false );
        if ( $template ) {
            $this->doActions();

            // phpcs:ignore WordPress.PHP.DontExtract.extract_extract
            extract( apply_filters( "flashpowder/template/context/{$this->getFilename()}", $context ) );

            require $template;

            return;
        }

        // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
        throw new FileNotFoundException( "Template file [{$this->getRelativePath()}] cannot be located." );
    }

    /**
     * Calls before including template actions.
     *
     * @return void
     */
    public function doActions() {
        if ( $this->isNamed() ) {
            list( $slug, $name ) = $this->file;

            do_action( "get_template_part_{$slug}", $slug, $name );

            return;
        }

        // Use first template name, if template file is an array, but is not named.
        if ( is_array( $this->file ) && isset( $this->file[0] ) ) {
            return do_action( "get_template_part_{$this->file[0]}", $this->file[0], null );
        }

        do_action( "get_template_part_{$this->file}", $this->file, null );
    }

    /**
     * Gets absolute path to the template.
     *
     * @return string
     */
    public function getPath() {
        $directory = $this->config['paths']['directory'];

        return $directory . '/' . $this->getRelativePath();
    }

    /**
     * Gets template path within `resources/templates` directory.
     *
     * @return string
     */
    public function getRelativePath() {
        $templates = $this->config['directories']['templates'];

        $extension = $this->config['templates']['extension'];

        return $templates . '/' . $this->getFilename( $extension );
    }

    /**
     * Gets template name.
     *
     * @param string $extension The file extension.
     *
     * @return string
     */
    public function getFilename( $extension = '.php' ) {
        // If template is named, return joined template names.
        if ( $this->isNamed() ) {
            return join( '-', $this->file ) . $extension;
        }

        // Use first template name, if template file is an array, but is not named.
        if ( is_array( $this->file ) && isset( $this->file[0] ) ) {
            return "{$this->file[0]}{$extension}";
        }

        return apply_filters( 'flashpowder/template/filename', "{$this->file}{$extension}" );
    }

    /**
     * Checks if template has variant name.
     *
     * @return boolean
     */
    public function isNamed() {
        // If file is not array, then template is not named for sure.
        if ( ! is_array( $this->file ) ) {
            return false;
        }

        // Return false if template is named, but name is invalid.
        // A valid name...
        if (
            isset( $this->file[1] ) && is_bool( $this->file[1] ) // ...should be set and not be a boolean
            || null === $this->file[1] // ...or null value
            || empty( $this->file[1] ) // ...or empty string/array
        ) {
            return false;
        }

        return true;
    }

    /**
     * Sets the file path to the template.
     *
     * @param string $file The file path.
     *
     * @return self
     */
    public function setFile( $file ) {
        $this->file = $file;

        return $this;
    }

    /**
     * Gets the file path to the template.
     *
     * @return string
     */
    public function getFile() {
        return $this->file;
    }
}
