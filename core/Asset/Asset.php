<?php

/**
|------------------------------------------------------------------
| Asset
|------------------------------------------------------------------
|
| This file helps for managing assets located in the
| `resources/assets` folder.
|
 */

namespace Flashpowder\Core\Asset;

use Flashpowder\Core\Contract\ConfigInterface;
use Flashpowder\Core\Exception\FileNotFoundException;

/**
 * Asset Class
 *
 * Represents a single file in the resources folder.
 */
class Asset {
    /**
     * Theme config instance.
     *
     * @var Flashpowder\Core\Config\ConfigInterface
     */
    protected $config;

    /**
     * Asset file.
     *
     * @var string
     */
    protected $file;

    /**
     * Construct asset.
     *
     * @param Flashpowder\Core\Contract\ConfigInterface $config The theme config.
     */
    public function __construct( ConfigInterface $config ) {
        $this->config = $config;
    }

    /**
     * Get asset file URI.
     *
     * @return string
     *
     * @throws Flashpowder\Core\Exception\FileNotFoundException When the asset file cannot be found.
     */
    public function getURI() {
        $file = $this->getPublicPath();
        if ( $this->fileExists( $file ) ) {
            return $this->getPublicURI();
        }

        // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
        throw new FileNotFoundException( "Asset file [{$file}] cannot be located." );
    }

    /**
     * Get asset file path.
     *
     * @return string
     *
     * @throws Flashpowder\Core\Exception\FileNotFoundException When the asset file cannot be found.
     */
    public function getPath() {
        $file = $this->getPublicPath();
        if ( $this->fileExists( $file ) ) {
            return $file;
        }

        // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
        throw new FileNotFoundException( "Asset file [{$file}] cannot be located." );
    }

    /**
     * Gets asset URI path.
     *
     * @return string
     */
    public function getPublicURI() {
        $uri = $this->config['paths']['uri'];

        return $uri . '/' . $this->getRelativePath();
    }

    /**
     * Gets asset directory path.
     *
     * @return string
     */
    public function getPublicPath() {
        $directory = $this->config['paths']['directory'];

        return $directory . '/' . $this->getRelativePath();
    }

    /**
     * Gets asset relative path.
     *
     * @return string
     */
    public function getRelativePath() {
        $public = $this->config['directories']['public'];

        return $public . '/' . $this->file;
    }

    /**
     * Checks if asset file exists.
     *
     * @param string $file The path of the file to check.
     *
     * @return boolean
     */
    public function fileExists( $file ) {
        return file_exists( $file );
    }

    /**
     * Gets the Asset file.
     *
     * @return string
     */
    public function getFile() {
        return $this->file;
    }

    /**
     * Sets the Asset file.
     *
     * @param string $file The path of the file to set.
     *
     * @return self
     */
    public function setFile( $file ) {
        $this->file = $file;

        return $this;
    }
}
