<?php

/**
|------------------------------------------------------------------
| File Not Found Exception
|------------------------------------------------------------------
|
| This exception is thrown when the requested asset
| file cannot be found in the `public/` folder.
|
 */

namespace Flashpowder\Core\Exception;

use Exception;

/**
 * FileNotFoundException Class
 */
class FileNotFoundException extends Exception {
    // Add class definition.
}
