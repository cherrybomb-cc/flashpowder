<?php

/**
|------------------------------------------------------------------
| Binding Resolution Exception
|------------------------------------------------------------------
|
| This exception is thrown when the service container cannot
| find a registered service.
|
 */

namespace Flashpowder\Core\Exception;

use Exception;

/**
 * BindingResolutionException Class
 */
class BindingResolutionException extends Exception {
    // Add class definition.
}
