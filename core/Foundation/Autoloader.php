<?php

/**
|------------------------------------------------------------------
| Autoloader
|------------------------------------------------------------------
|
| This file does the autoloading of the files defined
| defined in the config file.
|
 */

namespace Flashpowder\Core\Foundation;

use Flashpowder\Core\Contract\ConfigInterface;
use Flashpowder\Core\Exception\FileNotFoundException;

/**
 * Autoload Class
 *
 * This class locates and loads the files defined in the config.
 */
class Autoloader {
    /**
     * Theme config instance.
     *
     * @var Flashpowder\Core\Contract\ConfigInterface
     */
    protected $config;

    /**
     * Construct autoloader.
     *
     * @param Flashpowder\Core\Contract\ConfigInterface $config The theme config.
     */
    public function __construct( ConfigInterface $config ) {
        $this->config = $config;
    }

    /**
     * Autoload registered files.
     *
     * @return void
     */
    public function register() {
        do_action( 'flashpowder/autoloader/before_load' );

        $this->load();

        do_action( 'flashpowder/autoloader/after_load' );
    }

    /**
     * Localize and autoload files.
     *
     * @return void
     *
     * @throws Flashpowder\Core\Exception\FileNotFoundException When the file cannot be located.
     */
    public function load() {
        foreach ( $this->config['autoload'] as $file ) {
            if ( ! locate_template( $this->getRelativePath( $file ), true, true ) ) {
                // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
                throw new FileNotFoundException( "Autoloaded file [{$this->getPath( $file )}] cannot be found. Please, check your autoloaded entries in `config/app.php` file." );
            }
        }
    }

    /**
     * Gets absolute file path.
     *
     * @param string $file The file entry from the config.
     *
     * @return string
     */
    public function getPath( $file ) {
        $file = $this->getRelativePath( $file );

        return $this->config['paths']['directory'] . '/' . $file;
    }

    /**
     * Gets file path within `theme` directory.
     *
     * @param string $file The file entry from the config.
     *
     * @return string
     */
    public function getRelativePath( $file ) {
        return $this->config['directories']['app'] . '/' . $file;
    }
}
