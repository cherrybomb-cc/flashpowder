<?php

/**
|------------------------------------------------------------------
| Theme
|------------------------------------------------------------------
|
| This is the main core file that represents the theme with all
| of their characteristics, services, etc.
|
 */

namespace Flashpowder\Core\Foundation;

use Closure;

use Flashpowder\Core\Exception\BindingResolutionException;

/**
 * Theme Class
 */
class Theme extends Singleton {
    /**
     * Collection of services.
     *
     * @var array The collection of services.
     */
    protected $services = [];

    /**
     * Collection of services factories.
     *
     * @var array
     */
    protected $factories = [];

    /**
     * Registry of deposited services.
     *
     * @var array
     */
    protected $deposit = [];

    /**
     * Bind service into collection.
     *
     * @param string  $key The name of the service.
     * @param Closure $service The service function.
     *
     * @return self
     */
    public function bind( $key, Closure $service ) {
        $this->services[ $key ] = $service;

        return $this;
    }

    /**
     * Bind factory into collection.
     *
     * @param string  $key The name of the factory service.
     * @param Closure $factory The service function.
     *
     * @return self
     */
    public function factory( $key, Closure $factory ) {
        $this->factories[ $key ] = $factory;

        return $this;
    }

    /**
     * Resolves service with parameters.
     *
     * @param mixed $abstract The name of the service.
     * @param array $parameters The parameters passed to the service.
     *
     * @return mixed
     */
    protected function resolve( $abstract, array $parameters ) {
        return call_user_func_array( $abstract, [ $this, $parameters ] );
    }

    /**
     * Resolve service from container.
     *
     * @param string $key The name of the service to be resolved.
     * @param array  $parameters The parameters passed to the service.
     *
     * @return mixed
     *
     * @throws Flashpowder\Core\Exception\BindingResolutionException When the service is not found.
     */
    public function get( $key, array $parameters = [] ) {
        // If service is a factory, we should always return a new instance.
        if ( isset( $this->factories[ $key ] ) ) {
            return $this->resolve( $this->factories[ $key ], $parameters );
        }

        // Otherwise, look for service in services collection.
        if ( isset( $this->services[ $key ] ) ) {
            // Resolve service if we don't have a deposit for this service.
            if ( ! isset( $this->deposit[ $key ] ) ) {
                $this->deposit[ $key ] = $this->resolve( $this->services[ $key ], $parameters );
            }

            return $this->deposit[ $key ];
        }

        // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
        throw new BindingResolutionException( "Unresolvable resolution. The [{$key}] binding is not registered." );
    }

    /**
     * Determine if the given service exists.
     *
     * @param string $key The service name to check.
     *
     * @return bool
     */
    public function has( $key ) {
        return isset( $this->services[ $key ] ) || isset( $this->factories[ $key ] );
    }

    /**
     * Removes service from the container.
     *
     * @param string $key The service name to forget.
     *
     * @return void
     */
    public function forget( $key ) {
        unset( $this->factories[ $key ], $this->services[ $key ] );
    }

    /**
     * Gets a service.
     *
     * @param string $key The service name to get.
     *
     * @return mixed
     */
    public function offsetGet( $key ) {
        return $this->get( $key );
    }

    /**
     * Sets a service.
     *
     * @param string  $key The service name to set.
     * @param Closure $service The service function.
     *
     * @return void
     *
     * @throws Flashpowder\Core\Exception\BindingResolutionException When the service is not a Closure.
     */
    public function offsetSet( $key, Closure $service ) {
        if ( ! is_callable( $service ) ) {
            // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
            throw new BindingResolutionException( "Service definition [{$service}] is not an instance of Closure." );
        }

        $this->bind( $key, $service );
    }

    /**
     * Determine if the given service exists.
     *
     * @param string $key The service name to check.
     *
     * @return bool
     */
    public function offsetExists( $key ) {
        return $this->has( $key );
    }

    /**
     * Unsets a service
     *
     * @param string $key The name of the service to unset.
     *
     * @return void
     */
    public function offsetUnset( $key ) {
        $this->forget( $key );
    }
}
