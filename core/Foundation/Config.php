<?php

/**
|------------------------------------------------------------------
| Config
|------------------------------------------------------------------
|
| This file handles the theme configuration.
|
 */

namespace Flashpowder\Core\Foundation;

use ArrayAccess;

use Flashpowder\Core\Contract\ConfigInterface;

/**
 * Config Class
 *
 * This class manages the configuration that is used along the theme.
 */
class Config implements ConfigInterface, ArrayAccess {
    /**
     * All of the configuration items.
     *
     * @var array
     */
    protected $items = [];

    /**
     * Create a new configuration repository.
     *
     * @param array $items The configuration items.
     *
     * @return void
     */
    public function __construct( array $items = [] ) {
        $this->items = $items;
    }

    /**
     * Determine if given configuration value exists.
     *
     * @param string $key The item name to check.
     *
     * @return bool
     */
    public function has( $key ) {
        return isset( $this->items[ $key ] );
    }

    /**
     * Get the specified configuration value.
     *
     * @param string $key The item name to be retrieved.
     * @param mixed  $default The default value to be used.
     *
     * @return mixed
     */
    public function get( $key, $default = null ) {
        if ( ! isset( $this->items[ $key ] ) ) {
            return $default;
        }

        return apply_filters( "flashpowder/config/get/{$key}", $this->items[ $key ] );
    }

    /**
     * Set a given configuration value.
     *
     * @param array|string $key The item name to be set.
     * @param mixed        $value The value of the item.
     *
     * @return void
     */
    public function set( $key, $value = null ) {
        $keys = is_array( $key ) ? $key : [ $key => $value ];

        foreach ( $keys as $key => $value ) {
            $this->items[ $key ] = apply_filters( "flashpowder/config/set/{$key}", $value );
        }
    }

    /**
     * Get all of the configuration items for the application.
     *
     * @return array
     */
    public function all() {
        return $this->items;
    }

    /**
     * Determine if the given configuration option exists.
     *
     * @param mixed $key The item name to check.
     *
     * @return bool
     */
    public function offsetExists( mixed $key ): bool {
        return $this->has( $key );
    }

    /**
     * Get a configuration option.
     *
     * @param string $key The item name to retrieve.
     *
     * @return mixed
     */
    public function offsetGet( $key ): mixed {
        return $this->get( $key );
    }

    /**
     * Set a configuration option.
     *
     * @param mixed $key The item name to set.
     * @param mixed $value The value to be assigned to the item.
     *
     * @return void
     */
    public function offsetSet( mixed $key, mixed $value ): void {
        $this->set( $key, $value );
    }

    /**
     * Unset a configuration option.
     *
     * @param mixed $key The item name to unset.
     *
     * @retun void
     */
    public function offsetUnset( mixed $key ): void {
        $this->set( $key, null );
    }
}
