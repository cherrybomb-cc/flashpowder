<?php

/**
|------------------------------------------------------------------
| Singleton
|------------------------------------------------------------------
|
| The singleton degisn pattern restricts the instatiation of
| a class to a singular instance.
|
 */

namespace Flashpowder\Core\Foundation;

use Exception;

/**
 * Singleton Class
 */
abstract class Singleton {
    /**
     * Store the unique reference of the instance.
     *
     * @var Singleton The reference to *Singleton* instance of this class.
     */
    protected static $instance;

    /**
     * Returns the *Singleton* instance of this class.
     *
     * @return Singleton The *Singleton* instance.
     */
    public static function getInstance() {
        if ( null === static::$instance ) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Private constructor to prevent creating a new instance of the
     * *Singleton* via the `new` operator from outside of this class.
     */
    private function __construct() {
    }

    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     *
     * @return void
     */
    private function __clone() {
    }

    /**
     * Prevent unserializing of the *Singleton* instance.
     *
     * @return void
     *
     * @throws Exception When trying to unserialize the instance.
     */
    public function __wakeup() {
        throw new Exception( 'Cannot unserialize singleton' );
    }
}
