<?php

/**
|------------------------------------------------------------------
| Config Interface
|------------------------------------------------------------------
|
| This is a simple interface provides basic methods to get, set
| and search items in the configuration of the application.
|
 */

namespace Flashpowder\Core\Contract;

interface ConfigInterface {
    /**
     * Get all of the configuration items for the application.
     *
     * @return array
     */
    public function all();

    /**
     * Get the speficied configuration value.
     *
     * @param string $key The item name to retrieve.
     * @param mixed  $default The value to use if not set.
     *
     * @return mixed
     */
    public function get( $key, $default );

    /**
     * Set a given configuration value.
     *
     * @param array|string $key The item name to set.
     * @param mixed        $value The item value to be assigned.
     *
     * @return void
     */
    public function set( $key, $value );

    /**
     * Determine if the given configuration value exists.
     *
     * @param string $key The key to look for.
     *
     * @return bool
     */
    public function has( $key );
}
