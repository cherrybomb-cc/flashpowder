<?php

/**
 |------------------------------------------------------------------
 | Header Controller
 |------------------------------------------------------------------
 |
 | Controller for outputting layout's opening markup. Template
 | rendered here should include `wp_head()` function call.
 |
 */

namespace Flashpowder\Theme\Header;

use function Flashpowder\Theme\App\template;

/**
 * Renders layout's head.
 *
 * @see resources/templates/layout/head.tpl.php
 */
template( 'layout/head' );
