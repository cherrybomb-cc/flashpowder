<?php

/**
 |------------------------------------------------------------------
 | Single Controller
 |------------------------------------------------------------------
 |
 | Think about theme template files as some sort of controllers
 | from MVC design pattern. They should link application
 | logic with your theme view templates files.
 |
 */

namespace Flashpowder\Theme\Single;

use function Flashpowder\Theme\App\template;

/**
 * Renders single post.
 *
 * @see resources/templates/single.tpl.php
 */
template( 'single' );
