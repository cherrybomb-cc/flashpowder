<?php

/**
 |------------------------------------------------------------------
 | Sidebar Controller
 |------------------------------------------------------------------
 |
 | The template controller for displaying website sidebar,
 | i.e. area where you can assign various widgets.
 |
 */

namespace Flashpowder\Theme\Sidebar;

use function Flashpowder\Theme\App\template;

/**
 * Renders sidebar.
 *
 * @see resources/templates/partials/sidebar.tpl.php
 */
template( 'partials/sidebar' );
