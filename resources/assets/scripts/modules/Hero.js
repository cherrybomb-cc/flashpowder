import { module as BaseModule } from 'modujs';

export default class extends BaseModule {
  constructor(m) {
    super(m);

    this.events = {
      click: {
        button: 'changeTitle'
      }
    };
  }

  changeTitle() {
    this.$('title')[0].textContent = this.$('button')[0].value;
  }
}
