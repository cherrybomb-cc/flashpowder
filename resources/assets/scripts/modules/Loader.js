import { module as BaseModule } from 'modujs';
import barba from '@barba/core';
import gsap from 'gsap';

export default class extends BaseModule {
  constructor(m) {
    super(m);
  }

  init() {
    barba.init({
      debug: true,
      transitions: [
        {
          name: 'default-transition',
          //beforeOnce(data) {},
          //once(data) {},
          //afterOnce(data) {},
          //before(data) {},
          //beforeLeave(data) {},
          leave({current}) {
            return gsap.to(current.container, {
              opacity: 0,
            });
          },
          //afterLeave(data) {},
          //beforeEnter(data) {},
          enter({next}) {
            return gsap.from(next.container, {
              opacity: 0,
            });
          },
          //afterEnter(data) {}
          //after(data) {},
        }
      ]
    });

    barba.hooks.afterLeave(data => {
      this.call('destroy', data.current.container, 'app');
    });

    barba.hooks.afterEnter(data => {
      this.call('update', data.next.container, 'app');
    });
  }
}
