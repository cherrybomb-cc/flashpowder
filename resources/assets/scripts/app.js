import modular from 'modujs';
import * as modules from './modules';
import globals from './globals';
import { $html } from './utils/dom';
import { CSS_CLASS } from './config';

const app = new modular({
  modules: modules,
});

function init() {
  globals();

  app.init(app);

  $html.classList.add(CSS_CLASS.LOADED);
  $html.classList.add(CSS_CLASS.READY);
  $html.classList.remove(CSS_CLASS.LOADING);
}

window.addEventListener('DOMContentLoaded', (event) => {
  $html.classList.add(CSS_CLASS.DOM_READY);
});

window.addEventListener('load', () => {
  const $style = document.getElementById('app-css');

  if ($style) {
    if ($style.isLoaded) {
      init();
    } else {
      $style.addEventListener('load', () => init());
    }
  } else {
    console.warn('The #app-css stylesheet not found');
  }
});
