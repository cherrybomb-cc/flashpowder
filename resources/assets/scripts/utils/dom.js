const $html = document.documentElement;
const $body = document.body;
const isDebug = !!$html.getAttribute('data-debug');

export { $html, $body, isDebug };
