const isObject = (x) => x && typeof x === 'object';

const isFunction = (x) => typeof x === 'function';

const isNumeric = (x) => !isNaN(parseFloat(x)) && isFinite(x);

export { isFunction, isObject, isNumeric };
