const isIE11 = !!window.MSInputMethodContext && !!document.documentMode;
const isEDGE = window.navigator.userAgent.indexOf('Edge/') > -1;

export { isIE11, isEDGE };
