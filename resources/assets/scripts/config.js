const NODE_ENV = process.env.NODE_ENV;
const IS_MOBILE = window.matchMedia('(any-pointer:coarse)').matches;

// Main environment variables
const ENV = Object.freeze({
  // Node environment
  NAME: NODE_ENV,
  IS_PROD: NODE_ENV === 'production',
  IS_DEV: NODE_ENV === 'development',

  // Device
  IS_MOBILE,
  IS_DESKTOP: !IS_MOBILE
});

// Main CSS classes used within the project
const CSS_CLASS = Object.freeze({
  LOADING: 'is-loading',
  LOADED: 'is-loaded',
  READY: 'is-ready',
  DOM_READY: 'is-dom-ready',
  VISIBLE: 'is-visible',
  MENU_OPEN: 'has-menu-open',
});

// Custom JS events
const CUSTOM_EVENT = Object.freeze({
  //
});

export { ENV, CSS_CLASS, CUSTOM_EVENT };
