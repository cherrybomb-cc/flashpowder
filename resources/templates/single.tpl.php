<?php get_header(); ?>

<section class="c-singleEntry o-section">
  <div class="o-container">
    <div class="c-content">
      <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : ?>
          <?php the_post(); ?>
          <?php
            /**
             * Functions hooked into `theme/single/content` action.
             *
             * @hooked Flashpowder\Theme\App\Structure\render_post_content - 10
             */
            do_action( 'theme/single/content' );
          ?>
        <?php endwhile; ?>
      <?php endif; ?>
    </div>

    <?php if ( apply_filters( 'theme/single/sidebar/visibility', true ) ) : ?>
      <?php
        /**
         * Functions hooked into `theme/single/sidebar` action.
         *
         * @hooked Flashpowder\Theme\App\Structure\render_sidebar - 10
         */
        do_action( 'theme/single/sidebar' );
      ?>
    <?php endif; ?>
  </div>
</section>

<?php get_footer(); ?>
