<a href="<?= esc_attr( $attributes['href'] ) ?>">
    <?= wp_kses_post( $content ) ?>
</a>
