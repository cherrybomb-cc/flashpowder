<?php get_header(); ?>

<?php
  /**
   * Functions hooked into `theme/index/hero` action.
   *
   * @hooked Flashpowder\Theme\Index\render_hero - 10
   */
  do_action( 'theme/index/hero' );
?>

<section class="c-latest o-section">
  <div class="o-container">
    <?php if ( have_posts() ) : ?>
      <div class="c-latest_content">
        <h2>Posts</h2>

        <div class="c-posts">
          <?php while ( have_posts() ) : ?>
            <?php the_post(); ?>
            <?php
              /**
               * Functions hooked into `theme/index/post/thumbnail` action.
               *
               * @hooked Flashpowder\Theme\App\Structure\render_post_thumbnail - 10
               */
              do_action( 'theme/index/post/thumbnail' );
            ?>
          <?php endwhile; ?>
        </div>
      </div>
    <?php else : ?>
      <?php
        /**
         * Functions hooked into `theme/index/content/none` action.
         *
         * @hooked Flashpowder\Theme\App\Structure\render_empty_content - 10
         */
        do_action( 'theme/index/content/none' );
      ?>
    <?php endif; ?>
  </div>
</section>

<?php get_footer(); ?>
