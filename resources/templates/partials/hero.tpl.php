<div class="c-hero  o-section" data-module-hero>
  <div class="c-hero_content">
    <h1 data-hero="title"><?= wp_kses_post( $title ) ?></h1>
    <p><?= wp_kses_post( $text ) ?></p>
    <button data-hero="button" class="c-button" type="button" value="Title updated through the button"><?= wp_kses_post( $button_text ) ?></button>
  </div>
</div>
