<form action="/" method="GET">
  <label for="search">
    <input id="search" type="text" name="s" value="<?php the_search_query(); ?>" placeholder="Searching for...">
  </label>

  <input type="submit" value="Search">
</form>
