<article class="c-post thumbnail">
  <a href="<?php the_permalink(); ?>">
    <h3><?php the_title(); ?></h3>
  </a>

  <time datetime="<?= get_the_date( 'Y-m-d' ) ?>">
    <?php the_date( '' ); ?>
  </time>
</article>
