<header class="header">
  <h2><?= wp_kses( $title, 'strip' ) ?></h2>
  <p><?= wp_kses( $lead, 'strip' ) ?></p>

  <?php
    /**
     * Functions hooked into `theme/header/end` action.
     *
     * @hooked Flashpowder\Theme\App\Structure\render_documentation_button - 10
     */
    do_action( 'theme/header/end' );
  ?>
</header>
