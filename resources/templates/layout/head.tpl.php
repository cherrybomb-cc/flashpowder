<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="description" content="<?php bloginfo( 'description' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#000000">
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <?php wp_body_open(); ?>

    <div class="c-barba" data-barba="wrapper" data-module-loader>
      <header class="c-header">
        <p>[ BRAND LOGO ]</p>

        <nav class="c-nav">
          <?php
            // phpcs:disable
            wp_nav_menu( [
              'theme_location'  => 'primary',
              'container'       => '',
              'container_id'    => '',
              'container_class' => 'c-navContainer',
              'menu_id'         => 'primary-menu',
              'menu_class'      => 'c-navMenu',
              'walker'          => new Flashpowder\Theme\App\Structure\Menu_Walker(),
            ] );
            echo PHP_EOL;
            // phpcs:enable
          ?>
        </nav>
      </header>

      <main class="c-barba_container" data-barba="container" data-template="<?= Flashpowder\Theme\App\get_current_template(); ?>">
