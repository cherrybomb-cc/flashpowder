<?php

/**
 |------------------------------------------------------------------
 | Searchform Controller
 |------------------------------------------------------------------
 |
 | The template controller for displaying search form markup.
 | It is displayed when website posts was not found.
 |
 */

namespace Flashpowder\Theme\Searchform;

use function Flashpowder\Theme\App\template;

/**
 * Renders search form.
 *
 * @see resources/templates/partials/searchform.tpl.php
 */
template( 'partials/searchform' );
