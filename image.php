<?php

/**
 |------------------------------------------------------------------
 | Attached Images Controller
 |------------------------------------------------------------------
 |
 | This controller handles the mimetype for images.
 |
 */

namespace Flashpowder\Theme\Attachment\Image;

// Attachment page permanent redirection.
wp_redirect( get_permalink( $post->post_parent ), 301 );

exit;
