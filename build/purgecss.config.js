const config = {
  content: [
    './resources/templates/**/*.tpl.php',
    './resources/assets/scripts/**/*.js',
  ],
  safelist: [/^lenis\-?.*/, /^is\-.+/, /\-[a-z]+/],
};

module.exports = config;
