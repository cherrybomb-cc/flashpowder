const config = require('../app.config');

/**
 * External dependencies image files. This rule allows for emiting
 * image assets of external libraries to the application.
 */
module.exports = (isDev) => {
  return {
    test: /\.(png|jpe?g|gif|svg)$/,
    type: 'javascript/auto',
    include: config.paths.external,
    loader: 'file-loader',
    options: {
      //publicPath: config.paths.relative,
      name: config.outputs.external.image.filename,
    },
  };
};
