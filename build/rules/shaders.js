const config = require('../app.config');

/**
 * Internal application shaders files.
 */
module.exports = {
  test: /\.glsl$/,
  exclude: /node_modules/,
  loader: 'webpack-glsl-loader',
};
