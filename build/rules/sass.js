const autoprefixer = require('autoprefixer');
//const purgecss = require('@fullhuman/postcss-purgecss');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const config = require('../app.config');

/**
 * Internal application SASS files.
 * Have build-in autoprefixing.
 */
module.exports = (isDev) => {
  //let purgeCssConfig;

  //try {
  //  purgeCssConfig = require('../purgecss.config');
  //} catch (e) {
  //  purgeCssConfig = {
  //    content: ['**/*.tpl.php'],
  //  };
  //}

  return {
    test: /\.s[ac]ss$/,
    include: config.paths.sass,
    //exclude: /node_modules/,
    use: [
      MiniCssExtractPlugin.loader,
      {
        loader: 'css-loader',
        options: {
          sourceMap: isDev,
        },
      },
      {
        loader: 'postcss-loader',
        options: {
          sourceMap: isDev,
          postcssOptions: {
            plugins: [
              //purgecss( purgeCssConfig ),
              autoprefixer,
            ],
          },
        },
      },
      {
        loader: 'sass-loader',
        options: {
          sourceMap: isDev,
          //webpackImporter: false, // uncomment if there is any issues
          implementation: require('sass'),
          sassOptions: {
            outputStyle: isDev ? 'expanded' : 'compressed',
          },
        },
      },
    ],
  };
};
