const config = require('../app.config');

/**
 * Internal application image files. This rule exceptionally don't emit its files,
 * because they are handled by copy and image-minify webpack plugins.
 */
module.exports = {
  test: /\.(jpe?g|png|gif|svg)$/i,
  type: 'asset',
  include: config.paths.images,
  loader: 'file-loader',
  options: {
    name: config.outputs.image.filename,
    emitFile: false,
  },
};
