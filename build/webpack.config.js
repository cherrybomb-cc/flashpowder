const webpack = require('webpack');

const CopyPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
const ImageMinimizerPlugin = require('image-minimizer-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

const appConfig = require('./app.config');

const config = {
  /**
   * Application entry files for building.
   *
   * @type {Object}
   */
  entry: appConfig.assets,

  /**
   * Output settings for application scripts.
   *
   * @type {Object}
   */
  output: {
    path: appConfig.paths.public,
    filename: appConfig.outputs.javascript.filename,
  },

  /**
   * External objects which should be accessible inside application scripts.
   *
   * @type {Object}
   */
  externals: appConfig.externals,

  /**
   * Custom modules resolving settings.
   *
   * @type {Object}
   */
  resolve: appConfig.resolve,

  /**
   * Performance settings to speed up build times.
   *
   * @type {Object}
   */
  performance: {
    hints: false,
  },

  /**
   * Webpack DevServer settings
   *
   * @type {Object}
   */
  //devServer: {
  //  hot: true,
  //  host: 'localhost',
  //  port: 3000,
  //  //contentBase: '../public'
  //},

  /**
   * Common plugins which should run on every build.
   *
   * @type {Array}
   */
  plugins: [
    new webpack.ProgressPlugin(),
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin(appConfig.outputs.css),
    new ESLintPlugin(),
    new CopyPlugin({
      patterns: [
        {
          context: appConfig.paths.images,
          from: `${appConfig.paths.images}/**/*`,
          globOptions: {
            flatten: true,
            dot: false,
          },
          to: appConfig.outputs.image.filename,
        },
      ],
    }),
    //new CopyPlugin({
    //  patterns: [
    //    {
    //      context: appConfig.paths.media,
    //      from: `${appConfig.paths.media}/**/*`,
    //      globOptions: {
    //        flatten: true,
    //        dot: false,
    //      },
    //      to: appConfig.outputs.media.filename,
    //    },
    //  ],
    //}),
  ],
};

module.exports = (env, argv) => {
  const isDev = argv.mode === 'development' ? true : false;

  //const vueRule = require('./rules/vue');
  const sassRule = require('./rules/sass')(isDev);
  const fontsRule = require('./rules/fonts');
  const imagesRule = require('./rules/images');
  const shadersRule = require('./rules/shaders');
  const javascriptRule = require('./rules/javascript')(isDev);
  const externalFontsRule = require('./rules/external.fonts')(isDev);
  const externalImagesRule = require('./rules/external.images')(isDev);

  /**
   * Should the source map be generated?
   *
   * @type {String}
   */
  config.devtool =
    isDev && appConfig.settings.sourceMaps ? 'source-map' : undefined;

  /**
   * Build rules to handle application assset files.
   *
   * @type {Object}
   */
  config.module = {
    rules: [
      //vueRule,
      sassRule,
      fontsRule,
      imagesRule,
      shadersRule,
      javascriptRule,
      externalFontsRule,
      externalImagesRule,
    ],
  };

  /**
   * Adds Stylelint plugin if
   * linting is configured.
   */
  if (appConfig.settings.styleLint) {
    config.plugins.splice(
      -1,
      0,
      new StyleLintPlugin(appConfig.settings.styleLint),
    );
  }

  /**
   * Adds BrowserSync plugin when
   * settings are configured.
   */
  if (isDev && appConfig.settings.browserSync) {
    //config.plugins.splice(-1, 0,
    //  new webpack.HotModuleReplacementPlugin()
    //);

    config.plugins.splice(
      -1,
      0,
      new BrowserSyncPlugin(appConfig.settings.browserSync, {
        injectCss: true,
        // Prevent BrowserSync from reloading the page
        // and let Webpack Dev Server take care of this.
        reload: false,
      }),
    );
  }

  /**
   * Adds optimalizing plugins when
   * generating production build.
   */
  if (!isDev) {
    config.optimization = {
      minimizer: [
        new CssMinimizerPlugin(),
        new ImageMinimizerPlugin({
          minimizer: {
            implementation: ImageMinimizerPlugin.imageminMinify,
            options: {
              // Lossless optimization with custom option
              plugins: [
                ['gifsicle', { interlaced: true, optimizationLevel: 3 }],
                ['jpegtran', { progressive: true }],
                ['optipng', { optimizationLevel: 5 }],
                // SVGO configuration here https://github.com/svg/svgo#configuration
                [
                  'svgo',
                  {
                    plugins: [
                      {
                        name: 'preset-default',
                        params: {
                          overrides: {
                            addAttributesToSVGElement: {
                              params: {
                                attributes: [
                                  { 'version': '1.1' },
                                  { 'xmlns': 'http://www.w3.org/2000/svg' },
                                  { 'width': '100%' },
                                  { 'height': '100%' },
                                ],
                              },
                            },
                            cleanupIDs: false,
                            removeViewBox: false,
                            removeUselessDefs: false,
                            removeHiddenElems: false,
                            removeEmptyContainers: false,
                            removeUnknownsAndDefaults: false,
                          },
                        },
                      },
                    ],
                  },
                ],
              ],
            },
          }
        }),
      ],
    };
  }

  return config;
};
