<?php

/**
 |------------------------------------------------------------------
 | Index Controller
 |------------------------------------------------------------------
 |
 | Think about theme template files as some sort of controllers
 | from MVC design pattern. They should link application
 | logic with your theme view templates files.
 |
 */

namespace Flashpowder\Theme\Index;

use function Flashpowder\Theme\App\config;
use function Flashpowder\Theme\App\template;
use function Flashpowder\Theme\App\theme;

/**
 * Renders index page hero.
 *
 * @see resources/templates/index.tpl.php
 */
function render_hero()
{
    template(
        'partials/hero',
        [
            'title' => 'This is FlashPowder',
            'text' => 'FlashPowder is a WordPress Starter Theme. This is the hero section, a good place to have a catchy copy text...',
            'button_text' => '...and maybe a CTA',
        ]
    );
}
add_action( 'theme/index/hero', __NAMESPACE__ . '\\render_hero' );

/**
 * Renders index page.
 *
 * @see resources/templates/index.tpl.php
 */
template( 'index' );
